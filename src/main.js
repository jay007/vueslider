import Vue from 'vue'

import $ from 'jquery'

window.jQuery = window.$ = $;

require('bootstrap')
import 'bootstrap/dist/css/bootstrap.min.css'

import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
 